import request from 'supertest'
import { app, HTTP_STATUSES } from '../../src'
import { CreateCourseModel } from '../../src/models/create-course-model'
import { UpdateCourseModel } from '../../src/models/update-course-model'

describe('/course', () => {
    beforeAll(async () => {
        await request(app).delete('/__test__/data')
    })

    it('show return 200 and empty array', async () => {
        await request(app).get('/courses').expect(200, [])
    })

    it('show return 404 for not existing course', async () => {
        await request(app).get('/courses/1').expect(HTTP_STATUSES.NOT_FOUND_404)
    })

    it(`shouldn't  create course with incorrect input data`, async () => {
        const data: CreateCourseModel = { name: '' }
        await request(app)
            .post('/courses')
            .send(data)
            .expect(HTTP_STATUSES.NOT_FOUND_404)

        await request(app).get('/courses').expect(HTTP_STATUSES.OK_200, [])
    })

    let createdCourse: any = null

    it(`should  create course with correct input data`, async () => {
        const data: CreateCourseModel = { name: 'vlada course' }
        const createResponse = await request(app)
            .post('/courses')
            .send(data)
            .expect(HTTP_STATUSES.CREATED_201)

        createdCourse = createResponse.body

        expect(createdCourse).toEqual({
            id: expect.any(Number),
            name: data.name,
        })
        await request(app)
            .get('/courses')
            .expect(HTTP_STATUSES.OK_200, [createdCourse])
    })

    it(`shouldn't  update course with incorrect input data`, async () => {
        const data: UpdateCourseModel = { name: '' }
        await request(app)
            .put(`/courses/${createdCourse.id}`)
            .send(data)
            .expect(HTTP_STATUSES.BED_REQUEST_400)

        await request(app)
            .get('/courses')
            .expect(HTTP_STATUSES.OK_200, [createdCourse])
    })

    it(`shouldn't  update course that not exist`, async () => {
        const data: UpdateCourseModel = { name: 'bla bla' }
        await request(app)
            .put(`/courses/-2`)
            .send(data)
            .expect(HTTP_STATUSES.NOT_FOUND_404)

        await request(app)
            .get('/courses')
            .expect(HTTP_STATUSES.OK_200, [createdCourse])
    })
    let createdCourse2: any = null

    it(`should  update course with correct input data`, async () => {
        const data: UpdateCourseModel = { name: 'good name' }
        const createResponse = await request(app)
            .put(`/courses/${createdCourse.id}`)
            .send(data)
            .expect(HTTP_STATUSES.OK_200)

        createdCourse2 = createResponse.body
        expect(createdCourse2).toEqual(createdCourse2)
    })
})
