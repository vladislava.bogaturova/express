'use strict'
var __importDefault =
    (this && this.__importDefault) ||
    function (mod) {
        return mod && mod.__esModule ? mod : { default: mod }
    }
Object.defineProperty(exports, '__esModule', { value: true })
const express_1 = __importDefault(require('express'))
const app = (0, express_1.default)()
const port = 3000
const jsonBodyMiddleware = express_1.default.json()
app.use(jsonBodyMiddleware)
const db = {
    courses: [
        { id: 1, name: 'front-end' },
        { id: 2, name: 'back-end' },
        { id: 3, name: 'qa' },
    ],
}
const HTTP_STATUSES = {
    OK_200: 200,
    CREATED_201: 201,
    NO_CONTENT_204: 204,
    BED_REQUEST_400: 400,
    NOT_FOUND_404: 404,
}
app.get('/', (reg, res) => {
    res.json({ message: 'Hello world' })
})
app.get('/courses', (req, res) => {
    let foundCoursesQuery = db.courses
    if (req.query.name) {
        foundCoursesQuery.filter((c) => c.name.indexOf(req.query.name) > -1)
    }
    res.json(foundCoursesQuery)
})
app.get('/courses/:id', (req, res) => {
    const foundCourses = db.courses.find(
        (el) => el.id === Number(req.params.id)
    )
    if (!foundCourses) {
        res.sendStatus(HTTP_STATUSES.NOT_FOUND_404)
        return
    }
    res.json()
})
app.post('/courses', (req, res) => {
    if (!req.body.name) {
        res.sendStatus(HTTP_STATUSES.NOT_FOUND_404)
        return
    }
    const createdCourse = { id: +new Date(), name: req.body.name }
    db.courses.push(createdCourse)
    res.status(HTTP_STATUSES.CREATED_201).json(createdCourse)
})
app.delete('/courses/:id', (req, res) => {
    const filteredCourses = db.courses.filter(
        (el) => el.id !== Number(req.params.id)
    )
    if (filteredCourses.length === db.courses.length) {
        res.sendStatus(HTTP_STATUSES.BED_REQUEST_400)
        return
    }
    res.sendStatus(HTTP_STATUSES.NO_CONTENT_204)
})
app.put('/courses/:id', (req, res) => {
    if (!req.body.name) {
        res.sendStatus(HTTP_STATUSES.BED_REQUEST_400)
        return
    }
    const foundCourse = db.courses.find((el) => el.id === Number(req.params.id))
    if (!foundCourse) {
        res.sendStatus(HTTP_STATUSES.NOT_FOUND_404)
        return
    }
    foundCourse.name = req.body.name
    res.json(foundCourse)
})
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
