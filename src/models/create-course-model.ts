export type CreateCourseModel = {
    /**
     * Course name
     */
    name: string
}
