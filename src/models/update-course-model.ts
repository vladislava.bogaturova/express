export type UpdateCourseModel = {
    /**
     * Course name
     */
    name: string
}
