export type QueryCoursesModel = {
    /**
     * This title should be included in title for search
     */
    name: string
}
