export type CourseViewModel = {
    id: number
    name: string
}
