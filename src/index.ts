import express, { Response } from 'express'
import { CourseViewModel } from './models/course-view-model'
import { CreateCourseModel } from './models/create-course-model'
import { QueryCoursesModel } from './models/query-courses-model'
import { UpdateCourseModel } from './models/update-course-model'
import { URIParamsCourseIdModel } from './models/uri-params-course-id-model'
import {
    RequestWithBody,
    RequestWithParams,
    RequestWithParamsAndBody,
    RequestWithQuery,
} from './types'
export const app = express()
const port = 3000

const jsonBodyMiddleware = express.json()
app.use(jsonBodyMiddleware)

type CourseType = {
    id: number
    name: string
    studentCount: number
}

const db: { courses: CourseType[] } = {
    courses: [
        { id: 1, name: 'front-end', studentCount: 10 },
        { id: 2, name: 'back-end', studentCount: 10 },
        { id: 3, name: 'qa', studentCount: 10 },
    ],
}

const getCourseViewModel = (course: CourseType): CourseViewModel => {
    return {
        id: course.id,
        name: course.name,
    }
}

export const HTTP_STATUSES = {
    OK_200: 200,
    CREATED_201: 201,
    NO_CONTENT_204: 204,
    BED_REQUEST_400: 400,
    NOT_FOUND_404: 404,
}

app.get('/', (reg, res: Response) => {
    res.json({ message: 'Hello world' })
})

app.get(
    '/courses',
    (
        req: RequestWithQuery<QueryCoursesModel>,
        res: Response<CourseViewModel[]>
    ) => {
        const foundCoursesQuery = db.courses
        if (req.query.name) {
            foundCoursesQuery.filter((c) => c.name.indexOf(req.query.name) > -1)
        }

        res.json(foundCoursesQuery.map(getCourseViewModel))
    }
)
app.get(
    '/courses/:id',
    (req: RequestWithParams<URIParamsCourseIdModel>, res) => {
        const foundCourse = db.courses.find(
            (el) => el.id === Number(req.params.id)
        )

        if (!foundCourse) {
            res.sendStatus(HTTP_STATUSES.NOT_FOUND_404)
            return
        }
        res.json({
            id: foundCourse.id,
            name: foundCourse.name,
        })
    }
)

app.post(
    '/courses',
    (
        req: RequestWithBody<CreateCourseModel>,
        res: Response<CourseViewModel>
    ) => {
        if (!req.body.name) {
            res.sendStatus(HTTP_STATUSES.NOT_FOUND_404)
            return
        }
        const createdCourse: CourseType = {
            id: +new Date(),
            name: req.body.name,
            studentCount: 0,
        }
        db.courses.push(createdCourse)
        res.status(HTTP_STATUSES.CREATED_201).json(
            getCourseViewModel(createdCourse)
        )
    }
)

app.delete(
    '/courses/:id',
    (req: RequestWithParams<URIParamsCourseIdModel>, res) => {
        const filteredCourses = db.courses.filter(
            (el) => el.id !== Number(req.params.id)
        )

        if (filteredCourses.length === db.courses.length) {
            res.sendStatus(HTTP_STATUSES.BED_REQUEST_400)
            return
        }
        res.sendStatus(HTTP_STATUSES.NO_CONTENT_204)
    }
)

app.put(
    '/courses/:id',
    (
        req: RequestWithParamsAndBody<
            URIParamsCourseIdModel,
            UpdateCourseModel
        >,
        res: Response<CourseType>
    ) => {
        if (!req.body.name) {
            res.sendStatus(HTTP_STATUSES.BED_REQUEST_400)
            return
        }
        const foundCourse = db.courses.find(
            (el) => el.id === Number(req.params.id)
        )

        if (!foundCourse) {
            res.sendStatus(HTTP_STATUSES.NOT_FOUND_404)
            return
        }
        foundCourse.name = req.body.name
        res.json(foundCourse)
    }
)

app.delete('/__test__/data', (req, res) => {
    db.courses = []
    res.sendStatus(HTTP_STATUSES.NO_CONTENT_204)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
